import React, {useState, useEffect} from 'react';
import './App.css';
import SignInForm from './SignInForm';
import LoginForm from './LoginForm'
import WhoAmIForm from './WhoAmIForm'

function App() {
  const [user, setUser] = useState({})
  const [form, setForm] = useState("")

  useEffect(() => {
    const token = localStorage.getItem("token")
    if(token){
      fetch(`http://localhost:3001/auto_login`, {
        headers: {
          Authorization: `Bearer ${token}`
        }
      })
      .then(resp => resp.json())
      .then(data => {
        setUser(data)
        // console.log(data)
      })
    }
  }, [])

  const handleLogin = (user) => {
    setUser(user)
    handleFormSwitch('logout')
  }
  
  const handleRegistration = (user) => {
    setUser(user)
    handleFormSwitch('logout')
    alert('User has been successfully created')
  }

  const handleFormSwitch = (input) => {
    setForm(input)
  }

  const logOut = () => {
      localStorage.removeItem("token")
      handleFormSwitch('login')
  }

  const renderForm = () => {
    const token = localStorage.getItem("token")
    if(token){
      return <button className="ui button" onClick={logOut}>Log out</button> 
    }
    switch(form){
      case "login":
        return <LoginForm handleLogin={handleLogin} handleFormSwitch ={handleFormSwitch}/>
        break;
      default:
        return <SignInForm handleLogin={handleRegistration} handleFormSwitch ={handleFormSwitch}/>
    }
  }
  return (
    <div className="App">
        
        {
          renderForm()
        }
        <WhoAmIForm/>
    </div>
  );
}

export default App;
