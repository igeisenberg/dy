import React, {useState} from 'react'

function WhoAmIForm(props) {
    
    const whoAmI3001 = (evt) => {
        whoAmI(3001)
    }

    const whoAmI3002 = (evt) => {
        whoAmI(3002)
    }

    const whoAmI = (port) => {
        const token = localStorage.getItem("token")
        fetch(`http://localhost:` + port + `/who_am_i`, {
          headers: {
            "Authorization": `Bearer ${token}`
          }
        })
        .then(resp => resp.json())
        .then(data => console.log(data))
    }
    const formDivStyle = {
        margin: "auto",
        padding: "20px",
        width: "80%"
    }
    
    return(
        <div style={formDivStyle}>
            <div className="ui form">
                <div className="field">
                    <label>3001</label>
                    <button className="ui button" onClick={whoAmI3001}>Who Am I?</button>
                </div>
                <div className="field">
                    <label>3002</label>
                    <button className="ui button" onClick={whoAmI3002}>Who Am I?</button>
                </div>
  
               
            </div>
        </div>
    )
}

export default WhoAmIForm